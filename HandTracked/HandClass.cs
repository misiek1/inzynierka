﻿using System;
using System.Collections.Generic;
using Microsoft.Kinect;
using WMPLib;
using System.Windows.Controls;
using System.Threading;
using System.Windows.Threading;


namespace HandTracked
{
    public class HandClass
    {
        #region obiekty WMP
        WindowsMediaPlayer noteC3      = new WindowsMediaPlayer();
        WindowsMediaPlayer noteCSharp3 = new WindowsMediaPlayer();
        WindowsMediaPlayer noteD3      = new WindowsMediaPlayer();
        WindowsMediaPlayer noteDSharp3 = new WindowsMediaPlayer();
        WindowsMediaPlayer noteE3      = new WindowsMediaPlayer();
        WindowsMediaPlayer noteF3      = new WindowsMediaPlayer();
        WindowsMediaPlayer noteFSharp3 = new WindowsMediaPlayer();
        WindowsMediaPlayer noteG3      = new WindowsMediaPlayer();
        WindowsMediaPlayer noteGSharp3 = new WindowsMediaPlayer();
        WindowsMediaPlayer noteA3      = new WindowsMediaPlayer();
        WindowsMediaPlayer noteASharp3 = new WindowsMediaPlayer();
        WindowsMediaPlayer noteB3      = new WindowsMediaPlayer();
        WindowsMediaPlayer noteC4      = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteC3   = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteCSharp3  = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteD3  = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteDSharp3  = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteE3  = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteF3  = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteFSharp3  = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteG3  = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteGSharp3  = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteA3 = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteASharp3 = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteB3 = new WindowsMediaPlayer();
        WindowsMediaPlayer secondNoteC4 = new WindowsMediaPlayer();
        #endregion

        public void MusicLoadGuitar()
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(
            DispatcherPriority.Background,
            new Action(() =>
            {
                noteC3.URL = "GUITAR/GuitarC3OGG.wav";
                noteCSharp3.URL = "GUITAR/GuitarC#3OGG.wav";
                noteD3.URL = "GUITAR/GuitarD3OGG.wav";
                noteDSharp3.URL = "GUITAR/GuitarD#3OGG.wav";
                noteE3.URL = "GUITAR/GuitarE3OGG.wav";
                noteF3.URL = "GUITAR/GuitarF3OGG.wav";
                noteFSharp3.URL = "GUITAR/GuitarF#3OGG.wav";
                noteG3.URL = "GUITAR/GuitarG3OGG.wav";
                noteGSharp3.URL = "GUITAR/GuitarG#3OGG.wav";
                noteA3.URL = "GUITAR/GuitarA3OGG.wav";
                noteASharp3.URL = "GUITAR/GuitarA#3OGG.wav";
                noteB3.URL = "GUITAR/GuitarB3OGG.wav";
                noteC4.URL = "GUITAR/GuitarC4OGG.wav";
                secondNoteC3.URL = "GUITAR/2GuitarC3OGG.wav";
                secondNoteCSharp3.URL = "GUITAR/2GuitarC#3OGG.wav";
                secondNoteD3.URL = "GUITAR/2GuitarD3OGG.wav";
                secondNoteDSharp3.URL = "GUITAR/2GuitarD#3OGG.wav";
                secondNoteE3.URL = "GUITAR/2GuitarE3OGG.wav";
                secondNoteF3.URL = "GUITAR/2GuitarF3OGG.wav";
                secondNoteFSharp3.URL = "GUITAR/2GuitarF#3OGG.wav";
                secondNoteG3.URL = "GUITAR/2GuitarG3OGG.wav";
                secondNoteGSharp3.URL = "GUITAR/2GuitarG#3OGG.wav";
                secondNoteA3.URL = "GUITAR/2GuitarA3OGG.wav";
                secondNoteASharp3.URL = "GUITAR/2GuitarA#3OGG.wav";
                secondNoteB3.URL = "GUITAR/2GuitarB3OGG.wav";
                secondNoteC4.URL = "GUITAR/2GuitarC4OGG.wav";
                MusicStopFunction();
                MusicStopFunction2();
            }));
        }
        public void MusicLoadPiano()
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(
            DispatcherPriority.Background,
            new Action(() =>
            {
                noteC3.URL = "PIANO/PianoC3OGG.wav";
                noteCSharp3.URL = "PIANO/PianoC#3OGG.wav";
                noteD3.URL = "PIANO/PianoD3OGG.wav";
                noteDSharp3.URL = "PIANO/PianoD#3OGG.wav";
                noteE3.URL = "PIANO/PianoE3OGG.wav";
                noteF3.URL = "PIANO/PianoF3OGG.wav";
                noteFSharp3.URL = "PIANO/PianoF#3OGG.wav";
                noteG3.URL = "PIANO/PianoG3OGG.wav";
                noteGSharp3.URL = "PIANO/PianoG#3OGG.wav";
                noteA3.URL = "PIANO/PianoA3OGG.wav";
                noteASharp3.URL = "PIANO/PianoA#3OGG.wav";
                noteB3.URL = "PIANO/PianoB3OGG.wav";
                noteC4.URL = "PIANO/PianoC4OGG.wav";
                secondNoteC3.URL = "PIANO/2PianoC3OGG.wav";
                secondNoteCSharp3.URL = "PIANO/2PianoC#3OGG.wav";
                secondNoteD3.URL = "PIANO/2PianoD3OGG.wav";
                secondNoteDSharp3.URL = "PIANO/2PianoD#3OGG.wav";
                secondNoteE3.URL = "PIANO/2PianoE3OGG.wav";
                secondNoteF3.URL = "PIANO/2PianoF3OGG.wav";
                secondNoteFSharp3.URL = "PIANO/2PianoF#3OGG.wav";
                secondNoteG3.URL = "PIANO/2PianoG3OGG.wav";
                secondNoteGSharp3.URL = "PIANO/2PianoG#3OGG.wav";
                secondNoteA3.URL = "PIANO/2PianoA3OGG.wav";
                secondNoteASharp3.URL = "PIANO/2PianoA#3OGG.wav";
                secondNoteB3.URL = "PIANO/2PianoB3OGG.wav";
                secondNoteC4.URL = "PIANO/2PianoC4OGG.wav";
                MusicStopFunction();
                MusicStopFunction2();
            }));
        }

        public void Play(object PosY)
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(
            DispatcherPriority.Background,
            new Action(() =>
            {
                double PosYY = Convert.ToDouble(PosY);
                if (PosYY <= -0.40)
                {
                    if (noteC3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteC3.controls.play();
                    }
                }
                else if (PosYY >= -0.39 && PosYY <=-0.30)
                {
                    if (noteCSharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteCSharp3.controls.play();
                    }
                }
                else if (PosYY >= -0.29 && PosYY <= -0.20)
                {
                    if (noteD3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteD3.controls.play();
                    }
                }
                else if (PosYY >= -0.19 && PosYY <= -0.10)
                {
                    if (noteDSharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteDSharp3.controls.play();
                    }
                }
                else if (PosYY >= -0.09 && PosYY <= -0.00)
                {
                    if (noteE3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteE3.controls.play();
                    }
                }
                else if (PosYY >= 0.01 && PosYY <= 0.10)
                {
                    if (noteF3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteF3.controls.play();
                    }
                }
                else if (PosYY >= 0.11 && PosYY <= 0.20)
                {
                    if (noteFSharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteFSharp3.controls.play();
                    }
                }
                else if (PosYY >= 0.21 && PosYY <= 0.30)
                {
                    if (noteG3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteG3.controls.play();
                    }
                }
                else if (PosYY >= 0.31 && PosYY <= 0.40)
                {
                    if (noteGSharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteGSharp3.controls.play();
                    }
                }
                else if (PosYY >= 0.41 && PosYY <= 0.50)
                {
                    if (noteA3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteA3.controls.play();
                    }
                }
                else if (PosYY >= 0.51 && PosYY <= 0.60)
                {
                    if (noteASharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteASharp3.controls.play();
                    }
                }
                else if (PosYY >= 0.61 && PosYY <= 0.70)
                {
                    if (noteB3.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteB3.controls.play();
                    }
                }
                else if (PosYY >= 0.71)
                {
                    if (noteC4.playState != WMPPlayState.wmppsPlaying)
                    {
                        noteC4.controls.play();
                    }
                }
            }));
        }
        public void Play2(object PosY)
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(
            DispatcherPriority.Background,
            new Action(() =>
            {
                double PosYY = Convert.ToDouble(PosY);
                if (PosYY <= -0.40)
                {
                    if (secondNoteC3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteC3.controls.play();
                    }
                }
                else if (PosYY >= -0.39 && PosYY <= -0.30)
                {
                    if (secondNoteCSharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteCSharp3.controls.play();
                    }
                }
                else if (PosYY >= -0.29 && PosYY <= -0.20)
                {
                    if (secondNoteD3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteD3.controls.play();
                    }
                }
                else if (PosYY >= -0.19 && PosYY <= -0.10)
                {
                    if (secondNoteDSharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteDSharp3.controls.play();
                    }
                }
                else if (PosYY >= -0.09 && PosYY <= -0.00)
                {
                    if (secondNoteE3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteE3.controls.play();
                    }
                }
                else if (PosYY >= 0.01 && PosYY <= 0.10)
                {
                    if (secondNoteF3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteF3.controls.play();
                    }
                }
                else if (PosYY >= 0.11 && PosYY <= 0.20)
                {
                    if (secondNoteFSharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteFSharp3.controls.play();
                    }
                }
                else if (PosYY >= 0.21 && PosYY <= 0.30)
                {
                    if (secondNoteG3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteG3.controls.play();
                    }
                }
                else if (PosYY >= 0.31 && PosYY <= 0.40)
                {
                    if (secondNoteGSharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteGSharp3.controls.play();
                    }
                }
                else if (PosYY >= 0.41 && PosYY <= 0.50)
                {
                    if (secondNoteA3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteA3.controls.play();
                    }
                }
                else if (PosYY >= 0.51 && PosYY <= 0.60)
                {
                    if (secondNoteASharp3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteASharp3.controls.play();
                    }
                }
                else if (PosYY >= 0.61 && PosYY <= 0.70)
                {
                    if (secondNoteB3.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteB3.controls.play();
                    }
                }
                else if (PosYY >= 0.71)
                {
                    if (secondNoteC4.playState != WMPPlayState.wmppsPlaying)
                    {
                        secondNoteC4.controls.play();
                    }
                }
            }));
        }

        public void MusicStopFunction()
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(
            DispatcherPriority.Background,
            new Action(() =>
            {
                noteC3.controls.stop();
                noteCSharp3.controls.stop();
                noteD3.controls.stop();
                noteDSharp3.controls.stop();
                noteG3.controls.stop();
                noteGSharp3.controls.stop();
                noteE3.controls.stop();
                noteF3.controls.stop();
                noteFSharp3.controls.stop();
                noteA3.controls.stop();
                noteASharp3.controls.stop();
                noteB3.controls.stop();
                noteC4.controls.stop();
            }));
        }
        public void MusicStopFunction2()
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(
            DispatcherPriority.Background,
            new Action(() =>
            {
                secondNoteC3.controls.stop();
                secondNoteCSharp3.controls.stop();
                secondNoteD3.controls.stop();
                secondNoteDSharp3.controls.stop();
                secondNoteG3.controls.stop();
                secondNoteGSharp3.controls.stop();
                secondNoteE3.controls.stop();
                secondNoteF3.controls.stop();
                secondNoteFSharp3.controls.stop();
                secondNoteA3.controls.stop();
                secondNoteASharp3.controls.stop();
                secondNoteB3.controls.stop();
                secondNoteC4.controls.stop();
            }));
        }

        public void FunctionRightHand(IList<Body> _bodies, TextBlock tblRightHandState)
        {
            foreach (var body in _bodies)
            {
                if (body != null)
                {
                    if (body.IsTracked)
                    {
                        Joint handRight = body.Joints[JointType.HandRight];
                        Joint thumbRight = body.Joints[JointType.ThumbRight];

                        string rightHandState = "-";

                        float rightHandPosistionZ = (float)handRight.Position.Z;
                        float rightHandPosistionX = (float)handRight.Position.X;
                        float rightHandPosistionY = (float)handRight.Position.Y;
                        double rightPosX = Math.Round(rightHandPosistionX, 2);
                        double rightPosY = Math.Round(rightHandPosistionY, 2);
                        double rightPosZ = Math.Round(rightHandPosistionZ, 2);
                          
                        switch (body.HandRightState)
                        {
                            case HandState.Open:
                                rightHandState = "Open";
                                Thread threadPlay = new Thread(Play);
                                threadPlay.Start(rightPosY);
                                break;
                            case HandState.Closed:
                                rightHandState = "Closed";
                                Thread threadStop = new Thread(MusicStopFunction);
                                threadStop.Start();
                                break;
                            case HandState.Lasso:
                                rightHandState = "Lasso";
                                break;
                            case HandState.Unknown:
                                rightHandState = "Unknown";
                                break;
                            case HandState.NotTracked:
                                rightHandState = "Not Tracked";
                                break;
                            default:
                                break;
                        }
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(
                        DispatcherPriority.Background,
                        new Action(() => {
                            tblRightHandState.Text = rightHandState;
                        }));
                    }
                }
            }
        }
        
        public void FunctionLeftHand(IList<Body> _bodies, TextBlock tblLeftHandState)
        {
            foreach (var body in _bodies)
            {
                if (body != null)
                {
                    if (body.IsTracked)
                    {
                        Joint handLeft = body.Joints[JointType.HandLeft];
                        Joint thumbLeft = body.Joints[JointType.ThumbLeft];

                        string leftHandState = "-";

                        float leftHandPosistionX = (float)handLeft.Position.X;
                        float leftHandPosistionY = (float)handLeft.Position.Y;
                        float leftHandPosistionZ = (float)handLeft.Position.Z;
                        double leftPosX = Math.Round(leftHandPosistionX, 2);
                        double leftPosY = Math.Round(leftHandPosistionY, 2);
                        double leftPosZ = Math.Round(leftHandPosistionZ, 2);

                        switch (body.HandLeftState)
                        {
                            case HandState.Open:
                                leftHandState = "Open";
                                Thread threadPlay = new Thread(Play2);
                                threadPlay.Start(leftPosY);
                                break;
                            case HandState.Closed:
                                leftHandState = "Closed";
                                Thread threadStop = new Thread(MusicStopFunction2);
                                threadStop.Start();
                                break;
                            case HandState.Lasso:
                                leftHandState = "Lasso";
                                break;
                            case HandState.Unknown:
                                leftHandState = "Unknown";
                                break;
                            case HandState.NotTracked:
                                leftHandState = "Not Tracked";
                                break;
                            default:
                                break; 
                        }
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(
                        DispatcherPriority.Background,
                        new Action(() =>
                        {
                            tblLeftHandState.Text = leftHandState;
                        }));
                    }
                }
            }
        }
        
    }
}
