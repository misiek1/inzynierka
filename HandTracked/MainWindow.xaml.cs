﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using Microsoft.Kinect;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using HandTracked;

namespace HandTracked
{

    public partial class MainWindow : Window
    {
        KinectSensor _sensor;
        MultiSourceFrameReader _reader;
        Mode _mode = Mode.Color;
        IList<Body> _bodies;
        bool _displayBody = false;
        HandClass hand = new HandClass();
        private const string MediumGreyBrushKey = "MediumGreyBrush";
        private KinectAudioStream convertStream = null;
        private SpeechRecognitionEngine speechEngine = null;
        //private List<Span> recognitionSpans;

        public MainWindow()
        {
            InitializeComponent();
            //hand.MusicLoadGuitar();
        }

        private static RecognizerInfo TryGetKinectRecognizer()
        {
            IEnumerable<RecognizerInfo> recognizers;
            try
            {
                recognizers = SpeechRecognitionEngine.InstalledRecognizers();
            }
            catch (COMException)
            {
                return null;
            }

            foreach (RecognizerInfo recognizer in recognizers)
            {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) &&
                    "en-US".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return recognizer;
                }
            }

            return null;
        }

        public void posistionRight(KinectSensor _sensor)
        {
            foreach (var body in _bodies)
            {
                if (body != null)
                {
                    if (body.IsTracked)
                    {
                        Joint handRight = body.Joints[JointType.HandRight];
                        Joint thumbRight = body.Joints[JointType.ThumbRight];

                        canvas.DrawHand(handRight, _sensor.CoordinateMapper);
                        canvas.DrawThumb(thumbRight, _sensor.CoordinateMapper);

                        float rightHandPosistionX = (float)handRight.Position.X;
                        float rightHandPosistionY = (float)handRight.Position.Y;
                        float rightHandPosistionZ = (float)handRight.Position.Z;
                        double rightPosX = Math.Round(rightHandPosistionX, 2);
                        double rightPosY = Math.Round(rightHandPosistionY, 2);
                        double rightPosZ = Math.Round(rightHandPosistionZ, 2);

                        tblRightHandPositionX.Text = "X: " + rightPosX.ToString();
                        tblRightHandPositionY.Text = "Y: " + rightPosY.ToString();
                        tblRightHandPositionZ.Text = "Z: " + rightPosZ.ToString();
                    }
                }
            }
        }

        public void positionLeft(KinectSensor _sensor)
        {
            foreach (var body in _bodies)
            {
                if (body != null)
                {
                    if (body.IsTracked)
                    {
                        Joint handLeft = body.Joints[JointType.HandLeft];
                        Joint thumbLeft = body.Joints[JointType.ThumbLeft];

                        canvas.DrawHand(handLeft, _sensor.CoordinateMapper);
                        canvas.DrawThumb(thumbLeft, _sensor.CoordinateMapper);

                        float leftHandPosistionX = (float)handLeft.Position.X;
                        float leftHandPosistionY = (float)handLeft.Position.Y;
                        float leftHandPosistionZ = (float)handLeft.Position.Z;
                        double leftPosX = Math.Round(leftHandPosistionX, 2);
                        double leftPosY = Math.Round(leftHandPosistionY, 2);
                        double leftPosZ = Math.Round(leftHandPosistionZ, 2);

                        tblLeftHandPositionX.Text = "X: " + leftPosX.ToString();
                        tblLeftHandPositionY.Text = "Y: " + leftPosY.ToString();
                        tblLeftHandPositionZ.Text = "Z: " + leftPosZ.ToString();
                    }
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _sensor = KinectSensor.GetDefault();
            if (_sensor != null)
            {
                _sensor.Open();

                _reader = _sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | 
                    FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body);
                _reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;
            }
            else
            {
                MessageBox.Show("Kinect not Ready");
                Console.ReadKey();
                return;
            }
            
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (null != this.convertStream)
            {
                this.convertStream.SpeechActive = false;
            }

            if (null != this.speechEngine)
            {
                this.speechEngine.SpeechRecognized -= this.SpeechRecognized;
                this.speechEngine.SpeechRecognitionRejected -= this.SpeechRejected;
                this.speechEngine.RecognizeAsyncStop();
            }
            if (_reader != null)
            {
                _reader.Dispose();
            }

            if (_sensor != null)
            {
                _sensor.Close();
            }
        }
        
        private void ClearRecognitionHighlights()
        {

        }

        private void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            const double ConfidenceThreshold = 0.3;
            
            this.ClearRecognitionHighlights();

            if (e.Result.Confidence >= ConfidenceThreshold)
            {
                switch (e.Result.Semantics.Value.ToString())
                {
                    case "GUITAR":
                        hand.MusicLoadGuitar();
                        Check.Text = "Guitar";
                        break;
                    case "PIANO":
                        hand.MusicLoadPiano();
                        Check.Text = "Piano";
                        break;
                    case "START RECORDER":
                        Process.Start("C:\\Program Files\\Microsoft SDKs\\Kinect\\v2.0_1409\\Tools\\KinectStudio\\KStudio.exe");
                        break;
                }
            }
        }
        private void SpeechRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            this.ClearRecognitionHighlights();
        }

        private void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            var reference = e.FrameReference.AcquireFrame();
            
            #region COLOR, DEPTH & INFRARED FRAME
            using (var frame = reference.ColorFrameReference.AcquireFrame())
            {
                if(frame != null)
                {
                    if (_mode == Mode.Color)
                    {
                        camera.Source = frame.ToBitmap();
                    }
                }
            }
            using (var frame = reference.DepthFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    if (_mode == Mode.Depth)
                    {
                        camera.Source = frame.ToBitmap();
                    }
                }
            }
            using (var frame = reference.InfraredFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    if (_mode == Mode.Infrared)
                    {
                        camera.Source = frame.ToBitmap();
                    }
                }
            }
            #endregion
            
            using (var frame = reference.BodyFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    canvas.Children.Clear();
                    _bodies = new Body[frame.BodyFrameSource.BodyCount];
                    frame.GetAndRefreshBodyData(_bodies);

                    #region wyswietlanie linii węzłów
                    if (_displayBody)
                    {
                        foreach (var body in _bodies)
                        {
                            if (body != null)
                            {
                                if (body.IsTracked)
                                {
                                    if (_displayBody)
                                    {
                                        canvas.DrawSkeleton(body);
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    hand.FunctionRightHand(_bodies, tblRightHandState);
                    posistionRight(_sensor);
                    hand.FunctionLeftHand(_bodies, tblLeftHandState);
                    positionLeft(_sensor);
                }
            }
            
        }

        #region COLOR, DEPTH & INFRARED FRAME
        private void Depth_Click(object sender, RoutedEventArgs e)
        {
            _mode = Mode.Depth;
        }
        private void Infrared_Click(object sender, RoutedEventArgs e)
        {
            _mode = Mode.Infrared;
        }
        private void Color_Click(object sender, RoutedEventArgs e)
        {
            _mode = Mode.Color;
        }
        #endregion
        private void Body_Click(object sender, RoutedEventArgs e)
        {
            _displayBody = !_displayBody;
        }

        private void Instrument_Click(object sender, RoutedEventArgs e)
        {
            if (_sensor != null)
            {
                IReadOnlyList<AudioBeam> audioBeamList = this._sensor.AudioSource.AudioBeams;
                System.IO.Stream audioStream = audioBeamList[0].OpenInputStream();
                
                this.convertStream = new KinectAudioStream(audioStream);
            }
            else
            {
                MessageBox.Show("Kinect not Ready");
                Console.ReadKey();
                return;
            }

            RecognizerInfo ri = TryGetKinectRecognizer();
            
            if (null != ri)
            {
                this.speechEngine = new SpeechRecognitionEngine(ri.Id);

                var directions = new Choices();
                directions.Add(new SemanticResultValue("guitar", "GUITAR"));
                directions.Add(new SemanticResultValue("piano", "PIANO"));
                directions.Add(new SemanticResultValue("start recorder", "START RECORDER"));

                var gb = new GrammarBuilder { Culture = ri.Culture };
                gb.Append(directions);

                var g = new Grammar(gb);


                this.speechEngine.LoadGrammar(g);

                this.speechEngine.SpeechRecognized += this.SpeechRecognized;
                this.speechEngine.SpeechRecognitionRejected += this.SpeechRejected;

                this.convertStream.SpeechActive = true;

                this.speechEngine.SetInputToAudioStream(
                this.convertStream, new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
                this.speechEngine.RecognizeAsync(RecognizeMode.Multiple);
                
                MessageBox.Show("Wybierz instrument");
                this.convertStream.SpeechActive = false;
            }
            else
            {
                MessageBox.Show("No Speach recognizer");
                Console.ReadKey();
            }
            
        }
    }

    public enum Mode
    {
        Color,
        Depth,
        Infrared
    }
}


#region kod zakomentowany może się przydać
/*
    //Thread watek = new Thread(()=>cosik.FunctionRightHand(_bodies, frame, canvas, tblRightHandState));
    //watek.Start();
    //pozycjaPrawa();
    //Thread watek2 = new Thread(() => cosik.FunctionLeftHand(_bodies, frame, canvas, tblLeftHandState));
    //watek2.Start();
    //pozycjaLewa();

    #region DEPTH & INFRARED FRAME
    private void Depth_Click(object sender, RoutedEventArgs e)
    {
        _mode = Mode.Depth;
    }
    private void Infrared_Click(object sender, RoutedEventArgs e)
    {
        _mode = Mode.Infrared;
    }
    
    #region DEPTH & INFRARED FRAME
    using (var frame = reference.DepthFrameReference.AcquireFrame())
    {
        if (frame != null)
        {
            if (_mode == Mode.Depth)
            {
                camera.Source = frame.ToBitmap();
            }
        }
    }
    using (var frame = reference.InfraredFrameReference.AcquireFrame())
    {
        if (frame != null)
        {
            if (_mode == Mode.Infrared)
            {
                camera.Source = frame.ToBitmap();
            }
        }
    }

*/
#endregion

#region pętla do ciała
/*
foreach (var body in _bodies)
{
    if (body != null)
    {
        if (body.IsTracked)
        {
            Joint handRight = body.Joints[JointType.HandRight];
            Joint thumbRight = body.Joints[JointType.ThumbRight];
            Joint handLeft = body.Joints[JointType.HandLeft];
            Joint thumbLeft = body.Joints[JointType.ThumbLeft];

            canvas.DrawPoint(handRight);
            canvas.DrawPoint(handLeft);
            canvas.DrawPoint(thumbRight);
            canvas.DrawPoint(thumbLeft);

            string rightHandState = "-";
            string leftHandState = "-";

            float rightHandPosistionX = (float)handRight.Position.X;
            float rightHandPosistionY = (float)handRight.Position.Y;
            float rightHandPosistionZ = (float)handRight.Position.Z;
            double rightPosX = Math.Round(rightHandPosistionX, 2);      //dorobić funkcję konwertującą liczbe zmiennoprzecinkową
            double rightPosY = Math.Round(rightHandPosistionY, 2);
            double rightPosZ = Math.Round(rightHandPosistionZ, 2);

            float leftHandPosistionX = (float)handLeft.Position.X;
            float leftHandPosistionY = (float)handLeft.Position.Y;
            float leftHandPosistionZ = (float)handLeft.Position.Z;
            double leftPosX = Math.Round(leftHandPosistionX, 2);
            double leftPosY = Math.Round(leftHandPosistionY, 2);
            double leftPosZ = Math.Round(leftHandPosistionZ, 2);

            tblRightHandPositionX.Text = "X: " + rightPosX.ToString();
            tblRightHandPositionY.Text = "Y: " + rightPosY.ToString();
            tblRightHandPositionZ.Text = "Z: " + rightPosZ.ToString();

            tblLeftHandPositionX.Text = "X: " + leftPosX.ToString();
            tblLeftHandPositionY.Text = "Y: " + leftPosY.ToString();
            tblLeftHandPositionZ.Text = "Z: " + leftPosZ.ToString();

            switch (body.HandRightState)
            {
                case HandState.Open:
                    rightHandState = "Open";

                    #region pozycja reki kontrola i ustawienie dzwiekow
                    //player.controls.play();
                    //if(rightPosY== -0.50)
                    //{
                    //    player.controls.play();
                    //}else if(rightPosY == -0.40)
                    //{
                    //    MusicStopFunction();
                    //    player2.controls.play();
                    //}
                    //else if (rightPosY == -0.30)
                    //{
                    //    MusicStopFunction();
                    //    player3.controls.play();
                    //}
                    //else if (rightPosY == -0.20)
                    //{
                    //    MusicStopFunction();
                    //    player4.controls.play();
                    //}
                    //else if (rightPosY == -0.10)
                    //{
                    //    MusicStopFunction();
                    //    player5.controls.play();
                    //}
                    //else if (rightPosY == 0.00)
                    //{
                    //    MusicStopFunction();
                    //    player6.controls.play();
                    //}
                    //else if (rightPosY == 0.05)
                    //{
                    //    MusicStopFunction();
                    //    player7.controls.play();
                    //}
                    //else if (rightPosY == 0.10)
                    //{
                    //    MusicStopFunction();
                    //    player8.controls.play();
                    //}
                    //else if (rightPosY == 0.15)
                    //{
                    //    MusicStopFunction();
                    //    player9.controls.play();
                    //}
                    //else if (rightPosY == 0.20)
                    //{
                    //    MusicStopFunction();
                    //    player10.controls.play();
                    //}
                    //else if (rightPosY == 0.30)
                    //{
                    //    MusicStopFunction();
                    //    player11.controls.play();
                    //}
                    //else if (rightPosY == 0.40)
                    //{
                    //    MusicStopFunction();
                    //    player12.controls.play();
                    //}
                    //else if (rightPosY == 0.50)
                    //{
                    //    MusicStopFunction();
                    //    player13.controls.play();
                    //}
                    #endregion

                    break;
                case HandState.Closed:
                    rightHandState = "Closed";
                    MusicStopFunction();
                    break;
                case HandState.Lasso:
                    rightHandState = "Lasso";
                    break;
                case HandState.Unknown:
                    rightHandState = "Unknown";
                    break;
                case HandState.NotTracked:
                    rightHandState = "Not Tracked";
                    break;
                default:
                    break;
            }
            switch (body.HandLeftState)
            {
                case HandState.Open:
                    leftHandState = "Open";
                    //do klasy wrzucic, dzwieki do klasy, glowna klasa obsluga danej reki, kazda zeby miala swoj wątek, 
                    //jeszcze zobaczyc odswiezanie

                    #region pozycja reki kontrola i ustawienie dzwiekow
                    //player.controls.play();
                    //if (leftPosY == -0.50)
                    //{
                    //    player.controls.play();
                    //}
                    //else if (leftPosY == -0.40)
                    //{
                    //    MusicStopFunction();
                    //    player2.controls.play();
                    //}
                    //else if (leftPosY == -0.30)
                    //{
                    //    MusicStopFunction();
                    //    player3.controls.play();
                    //}
                    //else if (leftPosY == -0.20)
                    //{
                    //    MusicStopFunction();
                    //    player4.controls.play();
                    //}
                    //else if (leftPosY == -0.10)
                    //{
                    //    MusicStopFunction();
                    //    player5.controls.play();
                    //}
                    //else if (leftPosY == 0.00)
                    //{
                    //    MusicStopFunction();
                    //    player6.controls.play();
                    //}
                    //else if (leftPosY == 0.05)
                    //{
                    //    MusicStopFunction();
                    //    player7.controls.play();
                    //}
                    //else if (leftPosY == 0.10)
                    //{
                    //    MusicStopFunction();
                    //    player8.controls.play();
                    //}
                    //else if (leftPosY == 0.15)
                    //{
                    //    MusicStopFunction();
                    //    player9.controls.play();
                    //}
                    //else if (leftPosY == 0.20)
                    //{
                    //    MusicStopFunction();
                    //    player10.controls.play();
                    //}
                    //else if (leftPosY == 0.30)
                    //{
                    //    MusicStopFunction();
                    //    player11.controls.play();
                    //}
                    //else if (leftPosY == 0.40)
                    //{
                    //    MusicStopFunction();
                    //    player12.controls.play();
                    //}
                    //else if (leftPosY == 0.50)
                    //{
                    //    MusicStopFunction();
                    //    player13.controls.play();
                    //}
                    #endregion

                    break;
                case HandState.Closed:
                    leftHandState = "Closed";
                    MusicStopFunction();
                    break;
                case HandState.Lasso:
                    leftHandState = "Lasso";
                    break;
                case HandState.Unknown:
                    leftHandState = "Unknown";
                    break;
                case HandState.NotTracked:
                    leftHandState = "Not Tracked";
                    break;
                default:
                    break;
            }

            tblLeftHandState.Text = leftHandState;
            tblRightHandState.Text = rightHandState;

            //if (_displayBody)
            //{

            //    canvas.DrawSkeleton(body);

            //    //Joint head = new Joint();
            //    //head = body.Joints[JointType.HandLeft];

            //    //float x = head.Position.X;
            //    //float y = head.Position.Y;
            //    //float z = head.Position.Z;

            //    //cosX.Text ="X: " + x.ToString();
            //    //cosY.Text ="Y: " + y.ToString();
            //    //cosZ.Text ="Z: " + z.ToString();

            //}
        }
    }
}
*/
#endregion