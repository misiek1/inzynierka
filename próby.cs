
        List<WindowsMediaPlayer> listMusic = new List<WindowsMediaPlayer>();
        string[] tablicaDzwiekow=new string[13];
        double[] tabb = new double[13];
        double[] wynik = new double[13];

        public void functionLoad()
        {
            tablicaDzwiekow[0] = "C3OGG.wav";
            tablicaDzwiekow[1] = "C#3OGG.wav";
            tablicaDzwiekow[2] = "D3OGG.wav";
            tablicaDzwiekow[3] = "D#3OGG.wav";
            tablicaDzwiekow[4] = "E3OGG.wav";
            tablicaDzwiekow[5] = "F3OGG.wav";
            tablicaDzwiekow[6] = "F#3OGG.wav";
            tablicaDzwiekow[7] = "G3OGG.wav";
            tablicaDzwiekow[8] = "G#3OGG.wav";
            tablicaDzwiekow[9] = "A3OGG.wav";
            tablicaDzwiekow[10] = "A#3OGG.wav";
            tablicaDzwiekow[11] = "B3OGG.wav";
            tablicaDzwiekow[12] = "C4OGG.wav";
            for(int i = 0; i < tablicaDzwiekow.Length; i++)
            {
                listMusic[i].URL = tablicaDzwiekow[i];
                listMusic[i].controls.stop();
            }
        }

        public void tablice()
        {
            tabb[0] = -0.5;
            wynik[0] = tabb[0];
            double dd = 0.1;

            for (int i = 1; i < 13; i++)
            {
                tabb[i] = tabb[i - 1] + dd;
                wynik[i] = Math.Round(tabb[i], 1);
            }
        }

        public void PositionPlay(object PosY)
        {
            double PosYY = Convert.ToDouble(PosY);
            for (int i = 0; i < wynik.Length; i++)
            {
                if (wynik[i] == PosYY)
                {
                    for(int j = 0; j < listMusic.Count; j++)
                    {
                        if (i == j)
                        {
                            if (listMusic[j].playState != WMPPlayState.wmppsPlaying)
                            {
                                listMusic[j].controls.play();
                            }
                        }
                    }
                }
            }
        }