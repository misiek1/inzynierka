﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace consoleapplication
{
    public class Pracownik
    {
        // funkcja wirtualna
        virtual public void Pracuj() { Console.WriteLine("Pracownik.Pracuj()"); }
    }

    public class Sekretarka : Pracownik
    {
        // przesłaniamy
        override public void Pracuj() { Console.WriteLine("Sekretarka.Pracuj()"); }
    }

    class Program
    {
        private readonly string MUTEX_GUID = "e1ffff8f-c91d-4188-9e82-c92ca5b1d057";
        private Mutex m_oLoggerMutex = null;

        public Program()
        {
            m_oLoggerMutex = new Mutex(false, MUTEX_GUID);
        }

        public void Log()
        {
            m_oLoggerMutex.WaitOne();
            {
                StreamWriter oFile = null;
                try
                {
                    oFile = File.AppendText("logger.log");
                    oFile.WriteLine("Przykładowa linia...");
                    oFile.Flush();
                }
                finally
                {
                    if (null != oFile)
                    {
                        oFile.Close();
                        oFile.Dispose();
                    }
                }
            }
            m_oLoggerMutex.ReleaseMutex();
        }

        public void PositionPlay(object PosY)
        {
            double PosYY = Convert.ToDouble(PosY);
            double[] tabb = new double[13];
            tabb[0] = -0.6;
            tabb[1] = -0.6;
            for (int i = 1; i < tabb.Length - 1; i++)
            {
                tabb[i] += 0.1;
            }

        }

        static void Main(string[] args)
        {
            Sekretarka dd = new Sekretarka();
            Pracownik p = new Sekretarka();
            Pracownik pp = new Pracownik();
            p.Pracuj();
            dd.Pracuj();
            pp.Pracuj();
            // UWAGA! zwraca: Sekretarka.Pracuj();

            Console.ReadKey();

            //double[] tabb = new double[13];
            //double[] wynik = new double[13];
            //tabb[0] = -0.5;
            //wynik[0] = tabb[0];
            //double dd = 0.1;


            //for (int i = 1; i < 13; i++)
            //{
            //    tabb[i] = tabb[i - 1] + dd;
            //    wynik[i] = Math.Round(tabb[i], 1);
            //}
            //foreach (var item in wynik)
            //{
            //    Console.WriteLine(item);
            //}


            //double[] tabb = new double[13];
            //tabb[0] = -0.6;
            //tabb[1] = -0.6;
            //for (int i = 1; i < tabb.Length - 1; i++)
            //{
            //    tabb[i] =tabb[i-1] + 0.1;
            //    Console.WriteLine(tabb[i]);
            //}
            //Program cos = new Program();
            //cos.Log();



            //WĄTKI
            //MyThreadClass oMyThreadClass = new MyThreadClass();
            //Console.WriteLine("Wprowadź wzrost w m oraz wagę w kg w postaci \" wzrost;waga\"");
            //string sData = Console.ReadLine();
            //Thread oThread = new Thread(new ParameterizedThreadStart(oMyThreadClass.GetBmi));

            ////Thread oThread = new Thread(new ThreadStart(oMyThreadClass.Run));
            //Console.WriteLine("Oczekiwanie na zakonczenie watku...");
            //oThread.Start(sData);
            //oThread.Join();

            //ŚREDNIA
            //float liczba = 3.2353234f;
            //double wynik = Math.Round(liczba, 2);
            //Console.WriteLine(wynik);
            Console.ReadLine();
        }
    }
    
    class ExampleSingleton
    {
        private static volatile ExampleSingleton m_oInstance = null;
        private static readonly object m_oPadlock = new object();
        private string m_sTaskId = string.Empty;

        public static ExampleSingleton Instance
        {
            get
            {
                if (null == m_oInstance)
                {
                    lock (m_oPadlock)
                    {
                        if (null == m_oInstance)
                        {
                            m_oInstance = new ExampleSingleton();
                        }
                    }
                }
                return m_oInstance;
            }
        }
        public string TaskId
        {
            get
            {
                string sRetString = string.Empty;
                lock (m_oPadlock)
                {
                    sRetString = m_sTaskId;
                }
                return sRetString;
            }
            set
            {
                lock (m_oPadlock)
                {
                    m_sTaskId = value;
                }
            }
        }
    }

    //class Logger
    //{
    //    private readonly string MUTEX_GUID = "e1ffff8f-c91d-4188-9e82-c92ca5b1d057";
    //    private Mutex m_oLoggerMutex = null;

    //    public Logger()
    //    {
    //        m_oLoggerMutex = new Mutex(false, MUTEX_GUID);
    //    }

    //    public void Log()
    //    {
    //        m_oLoggerMutex.WaitOne();
    //        {
    //            StreamWriter oFile = null;
    //            try
    //            {
    //                oFile = File.AppendText("logger.log");
    //                oFile.WriteLine("Przykładowa linia...");
    //                oFile.Flush();
    //            }
    //            finally
    //            {
    //                if (null != oFile)
    //                {
    //                    oFile.Close();
    //                    oFile.Dispose();
    //                }
    //            }
    //        }
    //        m_oLoggerMutex.ReleaseMutex();
    //    }
    //}

    class MyThreadClass
    {
        public MyThreadClass() { }

        public void Run()
        {
            Console.WriteLine("Rozpoczynam prace...");
            Console.WriteLine("Test uspienia watku...");
            Thread.Sleep(500);
            Console.WriteLine("Koncze prace...");
        }

        public void GetBmi(object oData)
        {
            string[] asData = oData.ToString().Split(';');
            Console.WriteLine(string.Format("Twoja wartosc BMI: {0:f}", 
                              int.Parse(asData[1]) / Math.Pow(double.Parse(asData[0]), 2)));
            Console.WriteLine("Nacisnij dowolny znak...");
            Console.ReadKey();
        }
    }
}
