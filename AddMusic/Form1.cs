﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace AddMusic
{
    public partial class Form1 : Form
    {
        WindowsMediaPlayer player = new WindowsMediaPlayer();
        private SoundPlayer _soundPlayer;
        public Form1()
        {
            InitializeComponent();
            //player.URL = "09 DARK STAR METAL_C3OGG.wav";
            _soundPlayer = new SoundPlayer("METAL_C3OGG.wav");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //player.controls.play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //player.controls.stop();
        }

        private void checkBoxButton_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxButton.Checked)
            {
                checkBoxButton.Text = "Stop";
                _soundPlayer.PlayLooping();
               
              
            }else
            {
                checkBoxButton.Text = "Play";
                _soundPlayer.Stop();
                
            }
        }
    }
}
